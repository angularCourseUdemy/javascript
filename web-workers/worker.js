function fib(n) {
  if (n < 2) {
    return n;
  } else {
    return fib(n - 1) + fib(n - 2);
  }
}
function onMessage(evt) {
  const { index } = evt.data;
  console.log("INDEZ",index);
  const number = fib(index);
  self.postMessage({ index, number });
}
self.addEventListener("message", onMessage);
