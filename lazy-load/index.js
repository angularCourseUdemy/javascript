function intersection(entries, observer){
    entries.forEach(e => {
        if(e.isIntersecting){
            e.target.src = e.target.dataset.src
            observer.unobserve(e.target)
        }
        
    })
}

const options = {
    root: null,
    rootMargin: "0px",
    threshold: 0
}

const observer = new IntersectionObserver(intersection, options)

const img = document.querySelectorAll('img');
img.forEach(i => {
    observer.observe(i);
})