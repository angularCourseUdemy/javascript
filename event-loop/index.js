console.log("Tarea 1");

setTimeout(() => {
    console.log("Tarea 2");
})

Promise.resolve().then( () => {
    console.log("MicroTarea 1");
})

console.log("Tarea 3");

Promise.resolve().then( () => {
    console.log("MicroTarea 2");
})